package demo01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Jdbc01Main {

	public static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://localhost:3306/sh08";
	public static final String DB_USERNAME = "kishor";
	public static final String DB_PASSWORD = "kishor";
	
	static {
		try {
			// load and register driver class - one time activity per application
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public static void main(String[] args) {
		// Create connection
		try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)) {
			// prepare statement
			String sql = "SELECT id, name, author, subject, price FROM books";
			try (PreparedStatement stmt = connection.prepareStatement(sql)) {
				// execute statement: stsmt.executeQuery() or stmt.executeUpdate()
				try (ResultSet resultSet = stmt.executeQuery()) {
					// process result
					while(resultSet.next()) {
						int id = resultSet.getInt("id");
						String name = resultSet.getString("name");
						String author = resultSet.getString("author");
						String subject = resultSet.getString("subject");
						double price = resultSet.getDouble("price");
						System.out.printf("%d, %s, %s, %s, %f\n",
								id, name, author, subject, price);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
