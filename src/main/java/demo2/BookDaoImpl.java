package demo2;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements AutoCloseable {
	private Connection con;

	/**
	 * Create a DB connection
	 * @throws Exception
	 */
	public void open() throws Exception {
		con = DriverManager.getConnection(DBUtil.DB_URL, DBUtil.DB_USER, DBUtil.DB_PASSWORD);
	}

	/**
	 * Close DB connection
	 */
	@Override
	public void close() {
		try {
			if (con != null)
				con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get all the books
	 * @return
	 * @throws Exception
	 */
	public List<Book> findAllBooks() throws Exception {
		List<Book> list = new ArrayList<Book>();
		String sql = "SELECT id, name, author, subject, price FROM books";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			try(ResultSet rs = stmt.executeQuery()) {
				while(rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");
					String author = rs.getString("author");
					String subject = rs.getString("subject");
					double price = rs.getDouble("price");
					Book b = new Book(id, name, author, subject, price);
					list.add(b);
				}
			}
		}
		return list;
	}

	/**
	 * Add a new book
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public int addBook(Book b) throws Exception {
		int cnt = -1;
		String sql = "INSERT INTO books(id, name, author, subject, price) VALUES (?, ?, ?, ?, ?)";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setInt(1, b.getId());
			stmt.setString(2, b.getName());
			stmt.setString(3, b.getAuthor());
			stmt.setString(4, b.getSubject());
			stmt.setDouble(5, b.getPrice());
			cnt = stmt.executeUpdate();
		}
		return cnt;
	}

	/**
	 * Update existing book
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public int updateBook(Book b) throws Exception {
		int cnt = -1;
		String sql = "INSERT INTO books(id, name, author, subject, price) VALUES (?, ?, ?, ?) where id =?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, b.getName());
			stmt.setString(2, b.getAuthor());
			stmt.setString(3, b.getSubject());
			stmt.setDouble(4, b.getPrice());
			stmt.setInt(5, b.getId());
			cnt = stmt.executeUpdate();
		}
		return cnt;
	}

	/**
	 * Delete a book
	 * @param bookId
	 * @return
	 * @throws Exception
	 */
	public int deleteBook(int bookId) throws Exception {
		int cnt = -1;
		String sql = "DELETE FROM books where id =?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setInt(1, bookId);
			cnt = stmt.executeUpdate();
		}
		return cnt;
	}

	/**
	 * Get a book by ID
	 * @param bookId
	 * @return
	 * @throws Exception
	 */
	public Book findById(int bookId) throws Exception {
		String sql = "SELECT id, name, author, subject, price FROM books where id =?";
		Book book = null;
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setInt(1, bookId);
			try (ResultSet rs = stmt.executeQuery()) {
				if (rs != null) {
					while(rs.next()) {
						int id = rs.getInt("id");
						String name = rs.getString("name");
						String author = rs.getString("author");
						String subject = rs.getString("subject");
						double price = rs.getDouble("price");
						book = new Book(id, name, author, subject, price);
					}
				}
			}
		}
		return book;
	}

	/**
	 * Get all the books with matching subject
	 * @param bookSubject
	 * @return
	 * @throws Exception
	 */
	public List<Book> findBySubject(String bookSubject) throws Exception {
		String sql = "SELECT id, name, author, subject, price FROM books where subject =?";
		List<Book> list = new ArrayList<Book>();
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, bookSubject);
			try (ResultSet rs = stmt.executeQuery()) {
				if (rs != null) {
					while(rs.next()) {
						int id = rs.getInt("id");
						String name = rs.getString("name");
						String author = rs.getString("author");
						String subject = rs.getString("subject");
						double price = rs.getDouble("price");
						Book book = new Book(id, name, author, subject, price);
						list.add(book);
					}
				}
			}
		}
		return list;
	}

	/**
	 * Get all the available subjects
	 * @return
	 * @throws Exception
	 */
	public List<String> findSubjects() throws Exception {
		String sql = "SELECT DISTINCT(subject) FROM books";
		List<String> list = new ArrayList<String>();
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			try (ResultSet rs = stmt.executeQuery()) {
				if (rs != null) {
					while(rs.next()) {
						String subject = rs.getString("subject");
						list.add(subject);
					}
				}
			}
		}
		return list;
	}
}
